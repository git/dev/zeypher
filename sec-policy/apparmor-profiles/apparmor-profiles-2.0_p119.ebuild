# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils toolchain-funcs

MY_P="${P/_p/-}"
MY_S="${WORKDIR}/${P/_p*/}"
MONTH="October"

DESCRIPTION="AppArmor pre-built application encapsulation profiles."
HOMEPAGE="http://forge.novell.com/modules/xfmod/project/?apparmor"
SRC_URI="http://forgeftp.novell.com/apparmor/Development%20-%20${MONTH}%20Snapshot/${MY_P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE=""

DEPEND=""
RDEPEND="sys-apps/apparmor-parser"

pkg_postinst() {
	ewarn
	ewarn "DO NOT EXPECT THESE PROFILES TO WORK ON YOUR SYSTEM!!!"
	ewarn "	Expect that ENABLING THEM WILL BREAK things"
	ewarn " "
	ewarn "These profiles were built to work with a SuSE base install, and "
	ewarn "make many assumptions about file placement and system facilities "
	ewarn "that are quite possibly untrue on any Gentoo system ever emerged."
	ewarn " "
	ewarn "They are provided for reference purposes only, until profiles can "
	ewarn "be created and verified for Gentoo.  If you are interested in using "
	ewarn "AppArmor, and modifiying these base profiles for a Gentoo package, "
	ewarn "please feel free to do so, and contact the AppArmor package "
	ewarn "maintainer with your working profiles!"
	ewarn
	epause
}

src_unpack() {
	unpack ${A}

	cd ${MY_S}
	# Move profiles to a different dir so that none of 
	# them are sourced by default on startup.
	sed -i 's:apparmor.d$:apparmor.d/suse-defaults:g' Makefile
}

src_compile() {
	cd ${MY_S}
	emake CC="$(tc-getCC)" CFLAGS="${CFLAGS}" || die
}

src_install() {
	cd ${MY_S}
	# Place profiles in /usr/share, instead of /usr/src/Immunix
	MY_SHAREDIR="/usr/share/${PN}"
	make DESTDIR=${D} EXTRASDIR=${D}/${MY_SHAREDIR} install || die
}
