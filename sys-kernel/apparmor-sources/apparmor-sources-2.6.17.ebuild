# Copyright 1999-2005 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

ETYPE="sources"
K_WANT_GENPATCHES="base extras"
K_GENPATCHES_VER="1"
inherit eutils kernel-2
detect_version
detect_arch

KEYWORDS="~x86 ~amd64"
HOMEPAGE="http://forge.novell.com/modules/xfmod/project/?apparmor"
DESCRIPTION="Full sources to provide the required AppArmor modules and kernel hooks.  Based on the gentoo-sources tree."

MONTH="October"
UP_KPVER="2.6.17.9"

AA_SRC="http://forgeftp.novell.com/apparmor/Development%20-%20${MONTH}%20Snapshot/apparmor-kernel-patches-${UP_KPVER}.tar.gz"

SRC_URI="${KERNEL_URI} ${GENPATCHES_URI} ${ARCH_URI} ${AA_SRC}"

src_unpack() {
	unpack "apparmor-kernel-patches-${UP_KPVER}.tar.gz"

	kernel-2_src_unpack

	EPATCH_FORCE="yes"
	EPATCH_OPTS=""
	epatch ${WORKDIR}/${UP_KPVER}/current/apparmor-2.6.17.9-v154-fullseries.patch
}

pkg_postinst() {
	postinst_sources

	einfo
	einfo "For more info on this patchset, and how to report problems, see:"
	einfo "${HOMEPAGE}"
	einfo
	einfo "Make sure that your kernel configuration file is set with:"
	einfo "	CONFIG_SECURITY=y"
	einfo "		CONFIG_SECURITY_APPARMOR=m"
	einfo "Without these, apparmor will not function."
	einfo 
}
