# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils perl-module toolchain-funcs

MY_PV="${PV/_p/-}"
MY_P="${PN}-${MY_PV}"
MY_S="${WORKDIR}/${PN}-${PV/_p*/}"
MONTH="April"

DESCRIPTION="Documentation for AppArmor."
HOMEPAGE="http://forge.novell.com/modules/xfmod/project/?apparmor"
SRC_URI="http://forgeftp.novell.com/apparmor/Development%20-%20${MONTH}%20Snapshot/${MY_P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE="doc"

DEPEND=""
RDEPEND="${DEPEND}"

src_unpack() {
	unpack ${A}

	cd ${MY_S}
	sed -i "s:NOVELL/SUSE:Gentoo:g" Makefile
}

src_compile() {
	cd ${MY_S}
	emake CC="$(tc-getCC)" CFLAGS="${CFLAGS}" || die
}

src_install() {
	cd ${MY_S}
	make DESTDIR=${D} install_manpages || die 
	# Some files are missing from the doc distribution, so no '|| die'
	# bug submitted upstream
	make DESTDIR=${D} DOCDIR=/usr/share/doc/${P} install_documents
}
