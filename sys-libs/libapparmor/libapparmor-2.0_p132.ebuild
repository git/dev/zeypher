# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils toolchain-funcs

MY_P="${P/_p/-}"
MONTH="October"
DESCRIPTION="Primary support library and headers for AppArmor userspace"
HOMEPAGE="http://forge.novell.com/modules/xfmod/project/?apparmor"
SRC_URI="http://forgeftp.novell.com/apparmor/Development%20-%20${MONTH}%20Snapshot/${MY_P}.tar.gz"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE=""

DEPEND="virtual/libc"
RDEPEND="${DEPEND}"

MY_S=${WORKDIR}/${P/_*/}

src_unpack() {
	unpack ${A}

	cd ${MY_S}
	# the Make.rules isn't needed for Gentoo
	sed -i "s/^include Make.rules//g" Makefile
}

src_compile() {
	cd ${MY_S}
	emake CC="$(tc-getCC)" CFLAGS="${CFLAGS}" || die
}

src_install() {
	cd ${MY_S}
	make DESTDIR="${D}" install || die
}
