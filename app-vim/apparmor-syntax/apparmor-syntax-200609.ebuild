# Copyright 1999-2005 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit vim-plugin

DESCRIPTION="vim plugin: AppArmor policy syntax"
HOMEPAGE="http://forge.novell.com/modules/xfmod/project/?apparmor"
LICENSE="vim"
KEYWORDS="alpha amd64 ia64 mips ~ppc ppc64 sparc x86"
SRC_URI="mirror://gentoo/${P}.tar.bz2"
IUSE=""

VIM_PLUGIN_HELPTEXT="This plugin provides syntax highlighting for Apparmor
policy files."
