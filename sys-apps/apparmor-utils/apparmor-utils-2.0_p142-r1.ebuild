# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils perl-module toolchain-funcs

#MY_PN="apparmor-utils"
MY_PV="${PV/_p/-}"
MY_P="${PN}-${MY_PV}"
MY_S="${WORKDIR}/${PN}-${PV/_p*/}"
MONTH="October"

DESCRIPTION="AppArmor utilities for profile creation and management."
HOMEPAGE="http://forge.novell.com/modules/xfmod/project/?apparmor"
SRC_URI="http://forgeftp.novell.com/apparmor/Development%20-%20${MONTH}%20Snapshot/${MY_P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE="vim-syntax"

DEPEND="sys-libs/libapparmor
		sys-apps/apparmor-parser
		sys-libs/libcap
		dev-libs/libpcre
		dev-lang/perl
		perl-core/Test-Harness
		perl-core/Getopt-Long
		dev-perl/DBI 
		dev-perl/DBD-SQLite
		dev-perl/TimeDate
		dev-perl/File-Tail
		dev-perl/Locale-gettext"
RDEPEND="${DEPEND}
        app-doc/apparmor-docs
		vim-syntax? (app-vim/apparmor-syntax)"


src_unpack() {
	unpack ${A}
	cd ${MY_S}

	# Correct path for logger
	sed -i "s/\/bin\/logger/\/usr\/bin\/logger/g" genprof
}

src_compile() {
	cd ${MY_S}
	emake CC="$(tc-getCC)" CFLAGS="${CFLAGS}" || die
}

src_install() {
	cd ${MY_S}
	perlinfo
	make DESTDIR=${D} PERLDIR="${D}/${VENDOR_LIB}/Immunix" install || die
}
