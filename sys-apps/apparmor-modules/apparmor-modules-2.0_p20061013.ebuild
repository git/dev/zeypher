# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit linux-mod 

MY_S="${WORKDIR}/apparmor_modules/module"
MONTH="October"

DESCRIPTION="Kernel modules for AppArmor"
HOMEPAGE="ihttp://forge.novell.com/modules/xfmod/project/?apparmor"
SRC_URI="http://forgeftp.novell.com/apparmor/Development%20-%20${MONTH}%20Snapshot/apparmor-external-module.tar.gz"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE=""

DEPEND="virtual/libc"
RDEPEND="${DEPEND}
         sys-apps/apparmor-parser"

pkg_setup() {
	linux-info_pkg_setup

	if kernel_is lt 2 6 16 ; then
		eerror "You must have at least a 2.6.16 kernel to use this package."
		die "Insufficent kernel version"
	fi

	if linux_chkconfig_present CONFIG_SECURITY ; then
		eerror "CONFIG_SECURITY must be enabled in your kernel to build ${PN}"
		die "Bad kernel config"
	fi

	if (grep 'namespace_sem' ${KV_DIR}/include/linux/namespace.h >/dev/null 2>&1       && grep 'AUDIT_SD' ${KV_DIR}/include/linux/audit.h >/dev/null 2>&1 )
	then
		einfo "Required Symbol patch seems to have been applied; continuing."
	else
		eerror "Required symbols do not appear to be exported."
		ewarn
		ewarn "AppArmor modules requires a patch be applied against the"
		ewarn "kernel source to export several symbols."
		ewarn
		ewarn "Current patches can currently be found in: "
		ewarn "	${FILESDIR}"
		ewarn "where the clostest matching KERNEL_VER patch:"
		ewarn "	${P/_p*/}_[KERNEL_VER]_symbol_export.patch"
		ewarn "should apply cleanly."
		ewarn
		ewarn "After applying this patch and re-compiling,"
		ewarn "re-emerge this package"
		ewarn
		die "Missing apparmor symbol export patch"
	fi

}

src_unpack() {
	unpack ${A}
	cd "${MY_S}"

	sed -i 's/^all:/modules:/g' Makefile
	sed -i 's/mv/#mv/g' Makefile
}

src_compile() {
	cd "${MY_S}"
	unset ARCH
	CONFIG_SECURITY_APPARMOR=m make
}

src_install() {
	cd "${MY_S}"

	insinto /lib/modules/${KV_FULL}/kernel/security/apparmor
	doins apparmor.ko
	insinto /lib/modules/${KV_FULL}/kernel/security/apparmor/aamatch
	doins aamatch/aamatch_pcre.ko
}
