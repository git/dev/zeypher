# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils perl-module toolchain-funcs

MY_PV="${PV/_p/-}"
MY_P="${PN}-${MY_PV}"
MY_S="${WORKDIR}/${PN}-${PV/_p*/}"
MONTH="October"

DESCRIPTION="The userspace tools and init scripts to load security profiles into the apparmor kernel security module."
HOMEPAGE="http://forge.novell.com/modules/xfmod/project/?apparmor"
SRC_URI="http://forgeftp.novell.com/apparmor/Development%20-%20${MONTH}%20Snapshot/${MY_P}.tar.gz"


LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE=""

DEPEND="sys-libs/libapparmor
        sys-libs/libcap
        dev-libs/libpcre
        sys-devel/bison
        sys-devel/flex"

RDEPEND="|| ( sys-kernel/apparmor-sources sys-apps/apparmor-modules )"

src_unpack() {
	unpack ${A}

	## apparmor-parser
	cd ${MY_S}
	# the Make.rules isn't needed for Gentoo
	sed -i "s/^include Make.rules//g" Makefile
	# Un-needed historical artifact, AND ugly non-LSB path
	# This is mounted at /sys/kernel/security/subdomain, not /subdomain
	sed -i 's/^.*\/subdomain//g' Makefile
}

src_compile() {
	cd ${MY_S}
	emake CC="$(tc-getCC)" CFLAGS="${CFLAGS}" || die
}

src_install() {

	## apparmor-parser
	cd ${MY_S}
	make DESTDIR=${D} install || die

	## Init script and addtional files
	doinitd ${FILESDIR}/apparmor 
	doinitd ${FILESDIR}/aaeventd 
	insopts -m0644
	insinto /lib/apparmor
	doins ${FILESDIR}/rc.helper.functions
	doins ${FILESDIR}/rc.apparmor.functions

	dodir /etc/apparmor.d/abstractions
}
